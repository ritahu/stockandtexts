## stock price
library(quantmod)
getSymbols("BABA", scr="yahoo", from="2014-09-19", to="2015-09-28")
BABA
plot(BABA$BABA.Close, main = "alibaba stock price (from 2014-09-19 to 2015-09-28)")
save(BABA,file="阿里巴巴股票数据.Rdata")

## Data Visualization
library(ggplot2)
library(wordcloud)
library(Cairo)

## stock price
par(family="Fira Sans")
plot(BABA[,4], main = "Alibaba Stock Price (from 2014-09-23 to 2015-09-22)")

## Date-Sentiment bar plot
save(DateNP,file="日期正负情感矩阵.Rdata")
save(dns,file="datenbrsentiment.Rdata")
## DateNP是一个为了画bar plot建的矩阵
## 有3列Date P N
## 从2014-09-23开始到2015-09-22
## 一开始画出最基本的双向bar plot
## 正为绿色，负为红色
dim(DateApp)
datenp<-as.data.frame(cbind(Date=as.character(DateApp[,1]),Np,Nn))
Np
ggplot(datenp, aes(date))  +
  geom_bar(aes(y = -Nn), stat = "identity",fill="rosybrown1") + 
  geom_bar(aes(y = Np), stat = "identity",fill = "palegreen3")
class(datenp[,1])
date<-as.character(datenp[,1])
datenp<-data.frame(cbind(date,Np,Nn))
class(unlist(datenp[,1]))
## 横坐标太多了只取一部分
k<-1:25
last_plot()+scale_x_discrete(breaks=datenp[10*k,1],labels=datenp[10*k,1])
## 纵坐标
## 以及把下面负数的也换成正数
last_plot()+scale_y_discrete(limits=c(-7,-5,-1,0,1,5,7),breaks=c(-7,-5,-1,0,1,5,7),labels=c(-7,-5,-1,0,1,5,7))
## 横坐标标题格式
last_plot()+theme(axis.title.x = element_text(face="bold", colour="gray1", size=12))
## 纵坐标标题格式
last_plot()+ylab("Sentiment(Count)")
last_plot()+theme(axis.title.y = element_text(face="bold", colour="gray1", size=12))         
legend ("right" , c("setosa", "versicolor", "virginica") ,
        cex=1.5, pch=1:3)
## 番外
## 如果觉得"2014-10-22"这种格式太长了，可以替换格式
## 但这里长度够，所以我没替换
labels <- gsub("20([0-9]{2})-([0-9]{2})-([0-9]{2})", "\\1\n-\\2\n-\\3",
               date)

## 把两个图并在一块
## 但是要两个都是ggplot这种一层一层画上来的才能
library(gridExtra)
grid.arrange(a, b, nrow=2)

## word clouds
save(keep.IndexDate,file="序号日期矩阵.Rdata")
## 序号日期矩阵有两列
## 分别是 序号和日期
## 这样可以对应日期取到文章序号然后取到对应的词再统计词频生成词云
## 取初始序号
start<-as.numeric(as.character(keep.IndexDate$keep.index[max(which(keep.IndexDate$keep.Date=="2015-09-06"))]))
## 取结尾序号
end<-as.numeric(as.character(keep.IndexDate$keep.index[min(which(keep.IndexDate$keep.Date=="2015-09-22"))]))
w4cloud<-NULL
length(w4cloud)<-(start-end+1)
for(i in end:start){
  w4cloud[[i]]<-var2.words[[i]]  
}
## 词频
wordsNum <- table(unlist(w4cloud))
wordsNum <- sort(wordsNum) #排序
wordsData <- data.frame(words =names(wordsNum), freq = wordsNum)
Sep <- tail(wordsData,150) #取前150个词

## 1到150标个号 看什么词没意义 去掉
## 这个暂时只能手动操作
SN4cloud<-as.data.frame(cbind((1:150),Sep))
d<--c(2,4,7,8,13,14,15,25,34,35,37,40,41,44,45,57,59,60,63,66,68,76,83,89,95,96,99,106,112,113,114,126,127,131,137,138,139,140,141,145,146,148)
SN4cloud<-SN4cloud[d,]
Sepwords<-SN4cloud
save(Sepwords,file="Sepvocabulary.Rdata")

## create words clouds
## 设置字体为STHeiti
CairoFonts(regular = "STHeiti:style=Regular", bold = "STHeiti:style=Regular")
## 画布大小
Cairo(width = 6.5, height = 6.5, file = "Sep.wordcloud.png", bg = "white", units = "in", dpi = 600)
wordcloud(SN4cloud$words,SN4cloud$freq,col = brewer.pal(8, "RdBu"),random.order=F,random.color = T)
dev.off()





class((DateApp[,1]))
date<-x.date[-c(1:2,255:258)]
datanp<-data.frame(date=date)
datenp<-data.frame(date=labels)
sum<-rep(-1,540)
for(i in 1:540){
  sum[i]<-sum(rl.words[[i]]%in%"实现")
}
unlist(date)[which(sum!=0)]
which(rl.words[[321]]=="实现")
rl.words[[321]][394:433]
sentences[[321]]

################################
pairs.panels()
persp(cop_t_dim2,pCopula)
persp(cop_t_dim2,dCopula)
contour(cop_t_dim2,pCopula)
contour(cop_t_dim2,dCopula)
summary(cop_t_dim2)
