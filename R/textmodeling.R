setwd("~/Documents/Projects/stockandtexts/")
library(pogit)

## 由partition.R可以得到Data4Text
load("data/Data4Text.Rdata")
###################################
## Text Classification Modelling
###################################
## 因变量和自变量
Np<-Data4Text[,1]
Nn<-Data4Text[,2]
Nu<-Data4Text[,3]
X4text<-cbind(rep(1,252),Data4Text[,-(1:3)])

########################
## stepwiseBVS Function
########################
stepwiseBVS<-function(seed,y,X,ar,M,burnin){
  set.seed(seed)  ## 初次抽样
  nrow<-dim(X)[1]
  ncol<-dim(X)[2]
  index703<-sample(2:ncol,ncol-1)  ## 701个序号打乱
  index100<-index703[1:101]  ## 取前101个做第一次回归
  X100<-X[,index100]  ## 164*101矩阵
  Xd<-cbind(rep(1,nrow),X100)  ## 加上第一列是1作截距
  var.names<-colnames(X100)  ## 取出来的变量名字
  ## 第一个回归模型
  m1<-poissonBvs(y=y, X=Xd, mcmc = list(M = M,burnin=burnin), BVS = TRUE)
  m2<-summary(m1)
  select.var1<-which(m2$modTable[,2]>=ar) ## 接受率大于0.1的变量选进来
  n<-length(select.var1)
  if(n!=0)  
    {
      selvar1<-colnames(Xd)[select.var1] ## 选进来的向量名字
      acrate1<-m2$modTable[,2][select.var1] ## 引入概率
      beta1<-m2$modTable[,1][select.var1] ## beta值
      intercept1<-m2$modTable[,1][1] ## 截距
      keepvar1<-matrix(Xd[,select.var1],nrow=nrow) ## 保留的自变量 301*k
      colnames(keepvar1)<-selvar1
    }
  ## 计算AIC
  p1<-as.data.frame(cbind(y,keepvar1))
  tm1<-paste("y","~",sep=" ")
  tm2<-paste(selvar1,collapse="+")
  fam<-formula(paste(tm1,tm2,sep=""))
  mdl1<-glm(fam, family="poisson", data=p1)
  sum1<-summary(mdl1)
  AIC1<-sum1$aic

  ## 引进新100个变量
  i<-2
  while (i<=7) {
    index100<-index703[(100*(i-1)+2):(100*i+1)]
    X100<-X[,index100]
    Xd<-cbind(rep(1,nrow),X100,keepvar1)
    m3<-poissonBvs(y=y, X=Xd, mcmc = list(M = M,burnin=burnin), BVS = TRUE)
    m4<-summary(m3)
    select.var2<-which(m4$modTable[,2]>=ar) 
    n<-length(select.var2)
    if(n!=0)  
    {
      selvar2<-colnames(Xd)[select.var2] ## 选进来的向量名字
      acrate2<-m4$modTable[,2][select.var2] ## 引入概率
      beta2<-m4$modTable[,1][select.var2] ## beta值
      intercept2<-m4$modTable[,1][1] ## 截距
      keepvar2<-matrix(Xd[,select.var2],nrow=nrow) ## 保留的自变量 301*k
      colnames(keepvar2)<-selvar2
    }
  ## 计算AIC
    p2<-as.data.frame(cbind(y,keepvar2))
    tm1<-paste("y","~",sep=" ")
    tm2<-paste(selvar2,collapse="+")
    fam<-formula(paste(tm1,tm2,sep=""))
    mdl2<-glm(fam, family="poisson", data=p2)
    sum2<-summary(mdl2)
    AIC2<-sum2$aic

  ## 模型比较
    if (AIC2<AIC1) 
      { 
      m1<-m3 
      keepvar1<-keepvar2
      selvar1<-selvar2
      acrate1<-acrate2
      beta1<-beta2
      intercept1<-intercept2
      mdl1<-mdl2
      AIC1<-AIC2
      }
    i<-i+1
  }
  result.sum<-list(selvar=selvar1,acrate=acrate1,beta=beta1,
               intercept=intercept1,AIC=AIC1)
  return(result.sum)
}

###########
##并行计算
###########
library(parallel)
nc<-detectCores()
mc<-getOption("mc.cores", nc-1)
seed<-1:50
system.time(PredictP<-mclapply(seed,stepwiseBVS,Np,X4text,0.1,M=5000,burnin=1000,mc.cores = mc))
system.time(PredictU<-mclapply(seed,stepwiseBVS,Nu,X4text,0.1,M=5000,burnin=1000,mc.cores = mc))
system.time(PredictN<-mclapply(seed,stepwiseBVS,Nn,X4text,0.1,M=5000,burnin=1000,mc.cores = mc))

###########
##整理结果
###########
## load("data/PredictP.Rdata")
## load("data/PredictU.Rdata")
## load("data/PredictN.Rdata")
##1.把变量取出来
Pselvar.matrix<-matrix(rep("NA",50*60),nrow=50)
for(i in 1:30){
  n<-length(PredictP[[i]]$selvar)
  Pselvar.matrix[i,1:n]<-PredictP[[i]]$selvar
}
Uselvar.matrix<-matrix(rep("NA",30*60),nrow=30)
for(i in 1:30){
  n<-length(PredictU[[i]]$selvar)
  Uselvar.matrix[i,1:n]<-PredictU[[i]]$selvar
}
Nselvar.matrix<-matrix(rep("NA",50*60),nrow=50)
for(i in 19:27){
  n<-length(PredictN[[i]]$selvar)
  Nselvar.matrix[i,1:n]<-PredictN[[i]]$selvar
}

## 第5个整理结果
## Pvar.total
Pvar.total<-names(sort(table(Pselvar.matrix)))
Pvar.total<-Pvar.total[-86]
## Uvar.total
Uvar.total<-names(sort(table(Uselvar.matrix)))
Uvar.total<-Uvar.total[-326]
## which(sort(table(Uselvar.matrix))==3)
##[-(1:238)]
## Uvar.total<-Uvar.total[-(1:238)]
## Uvar.total<-Uvar.total[-88]
## Nvar.total
Nvar.total<-names(sort(table(Nselvar.matrix)))
Nvar.total<-Nvar.total[-234]
## Nvar.total<-ccc$selvar

#################
##确定最后的模型
################
######
##Fp
#####
Xp<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Pvar.total)])
M1<-poissonBvs(y=Np, X=Xp, mcmc = list(M = 1000,burnin=500), BVS = TRUE)
S1<-summary(M1)
## ar<-0.8
select.var<-which(S1$modTable[,2]>=ar) 
selvar<-colnames(Xp)[select.var] ## 选进来的向量名字
acrate<-S1$modTable[,2][select.var] ## 引入概率
beta<-S1$modTable[,1][select.var] ## beta值
intercept<-S1$modTable[,1][1] ## 截距
betaP<-beta
names(betaP)<-selvar
betaP<-c(intercept,betaP)
XP<-cbind(rep(1,252),X4text[1:252,which(colnames(X4text)%in%selvar)])
############################################
## lambda=exp(X%in%beta) F=ppois(N,lambda)
############################################
lambdaP<-exp(XP%*%betaP)
Fp<-ppois(Np[21:252],lambdaP[21:252])

######
##Fu
#####
Xu<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Uvar.total)])
M1<-poissonBvs(y=Nu, X=Xu, mcmc = list(M = 1000,burnin=500), BVS = TRUE)
S1<-summary(M1)
## ar<-0.8
select.var<-which(S1$modTable[,2]>=ar) 
selvar<-colnames(Xu)[select.var] ## 选进来的向量名字
acrate<-S1$modTable[,2][select.var] ## 引入概率
beta<-S1$modTable[,1][select.var] ## beta值
intercept<-S1$modTable[,1][1] ## 截距
betaU<-beta
names(betaU)<-selvar
betaU<-c(intercept,betaU)
XU<-cbind(rep(1,252),X4text[1:252,which(colnames(X4text)%in%selvar)])
############################################
## lambda=exp(X%in%beta) F=ppois(N,lambda)
############################################
lambdaU<-exp(XU%*%betaU)
Fu<-ppois(Nu[21:252],lambdaU[21:252])

######
##Fn
#####
Xn<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Nvar.total)])
M1<-poissonBvs(y=Nn, X=Xn, mcmc = list(M = 1000,burnin=500), BVS = TRUE)
S1<-summary(M1)
## ar<-0.8
select.var<-which(S1$modTable[,2]>=ar) 
selvar<-colnames(Xn)[select.var] ## 选进来的向量名字
acrate<-S1$modTable[,2][select.var] ## 引入概率
beta<-S1$modTable[,1][select.var] ## beta值
intercept<-S1$modTable[,1][1] ## 截距
betaN<-beta
names(betaN)<-selvar
betaN<-c(intercept,betaN)
XN<-cbind(rep(1,252),X4text[1:252,which(colnames(X4text)%in%selvar)])
############################################
## lambda=exp(X%in%beta) F=ppois(N,lambda)
############################################
lambdaN<-exp(XN%*%betaN)
Fn<-ppois(Nn[21:252],lambdaN[21:252])

###########
## 用glm
###########
data<-as.data.frame(cbind(Np,X4text[,which(colnames(X4text)%in%Pvar.total)]))
tm1<-paste("Np","~",sep=" ")
tm2<-paste(colnames(X4text[,which(colnames(X4text)%in%Pvar.total)]),collapse="+")
fam<-formula(paste(tm1,tm2,sep=""))
mdl2<-glm(fam, family="poisson", data=data)
sum2<-summary(mdl2)
betaP<-sum2$coefficients[,1]
XP<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Pvar.total)])
######################################################
data<-as.data.frame(cbind(Nu,X4text[,which(colnames(X4text)%in%Uvar.total)]))
tm1<-paste("Nu","~",sep=" ")
tm2<-paste(colnames(X4text[,which(colnames(X4text)%in%Uvar.total)]),collapse="+")
fam<-formula(paste(tm1,tm2,sep=""))
mdl2<-glm(fam, family="poisson", data=data)
sum2<-summary(mdl2)
betaU<-sum2$coefficients[,1]
XU<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Uvar.total)])
######################################################
data<-as.data.frame(cbind(Nn,X4text[,which(colnames(X4text)%in%Nvar.total)]))
tm1<-paste("Nn","~",sep=" ")
tm2<-paste(colnames(X4text[,which(colnames(X4text)%in%Nvar.total)]),collapse="+")
fam<-formula(paste(tm1,tm2,sep=""))
mdl2<-glm(fam, family="poisson", data=data)
sum2<-summary(mdl2)
betaN<-sum2$coefficients[,1]
XN<-cbind(rep(1,252),X4text[,which(colnames(X4text)%in%Nvar.total)])
#######################################################
sum(abs(lambdaP-Np))
sum(round(lambdaP)==Np)/252
sum(abs(lambdaN-Nn))
sum(round(lambdaN)==Nn)/252
sum(abs(lambdaU-Nu))
sum(round(lambdaU)==Nu)/252
sum(rep(0,252)==Nu)/252
system.time(ddd<-mclapply(seed,stepwiseBVS,Nn,X4text,0.95,M=1000,burnin=500,mc.cores = mc))
lp<-lambdaP[21:252]

