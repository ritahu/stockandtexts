setwd("~/Documents/Projects/stockandtexts/")

library(RCurl)
library(XML)
######################
## get.content(url): the parameter is the link of the article, the output is the content of the article 
get.content<-function(url){
  article.page <- getURL(url, encoding = "UTF-8")
  dv.article.page<- htmlParse(article.page, encoding = "UTF-8")
  content<-getNodeSet(dv.article.page, "//div[@id='Main_Content_Val']//p")
  par.n<-length(content)
  if(par.n==0) return("无")
  else {
    articles.content<-as.matrix(rep(0,par.n),nrow=1)
    for(i in 1:par.n){
      articles.content[i,] <- xmlValue(content[[i]])}
    return(articles.content)}
}

## get.info(url): the parameter is the link of the first page of the keyword, the output is a list, including dates, titles, and links of the articles
get.info<-function(url){
  result.page <- getURL(url, encoding = "UTF-8")
  dv.result.page <- htmlParse(result.page, encoding = "UTF-8")
  url.nodes <- getNodeSet(dv.result.page, "//div[@class='searchxt']//a")
  date.nodes <- getNodeSet(dv.result.page, "//div[@class='searchxt']//span")
  articles.n<-length(url.nodes)
  articles.url <- as.matrix(rep(0,articles.n))
  titles <- as.matrix(rep(0,articles.n))
  dates <- as.matrix(rep(0,articles.n))
  page.info <- list(articles.url=articles.url,titles=titles,dates=dates)
  for(i in 1:articles.n){
    if (is.null(url.nodes[[i]])) {
      page.info$articles.url[i,]<-NULL 
      page.info$titles[i,]<-NULL
      page.info$dates[i,]<-NULL
    }
    else {
      page.info$articles.url[i,] <- xmlGetAttr(url.nodes[[i]], name = "href")
      page.info$titles[i,] <- xmlValue(url.nodes[[i]])
      page.info$dates[i,] <- xmlValue(date.nodes[[i]])
    }
  }
  return(page.info)
}

## get.articles.number(url): the parameter is the link of the first page of the keyword, the output is the total number of the articles related to the keyword
get.articles.number<-function(url){
  result.page <- getURL(url, encoding = "UTF-8")
  dv.result.page <- htmlParse(result.page, encoding = "UTF-8")
  number.nodes <- getNodeSet(dv.result.page, "//div[@class='keyWordBox01']//b")
  return(as.numeric(xmlValue(number.nodes[[2]])))
}

## get.1page.url(x): the parameter is the keyword, eg. "阿里巴巴", the output is the the link of the first page of the keyword
get.1page.url<-function(x){
return(paste("http://search.caixin.com/search/search.jsp?special=false&keyword=",x,"&channel=0&type=1&sort=1&time=4&startDate=&endDate=&page=",
                         as.character(1),sep=""))}

## download.articles(x): the parameter is the keyword, the output is the articles in one year related to the keyword
download.articles<-function(x){
  articles.content.list<-list(NULL)
  length(articles.content.list)<-get.articles.number(get.1page.url(x))
  pages.n<-floor(length(articles.content.list)/20)
    for(k in 1:pages.n){
      ck=as.character(k)
      search.url<-paste("http://search.caixin.com/search/search.jsp?special=false&keyword=",x,"&channel=0&type=1&sort=1&time=4&startDate=&endDate=&page=",ck,sep="")
      articles.url<-get.info(search.url)$articles.url
      titles<-get.info(search.url)$titles
      dates<-get.info(search.url)$dates
      articles.content<-apply(articles.url,1,get.content)
      for(i in 1:20)
        articles.content.list[[(20*(k-1)+i)]]<-list(titles[i,],dates[i,],articles.content[[i]])
      }
  return(articles.content.list)
}

BABA.news<-download.articles("阿里巴巴")
##save(BABA.news,file="BABA.news.Rdata")

