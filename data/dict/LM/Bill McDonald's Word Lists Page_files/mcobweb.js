//UDMv3.8.1
//* DO NOT EDIT THIS BIT *
if (!exclude) { //********
//************************



///////////////////////////////////////////////////////////////////////////
//
//  ULTIMATE DROPDOWN MENU VERSION 3.8.1 by Brothercake
//  http://www.brothercake.com/dropdown/ 
//
//  Link-wrapping routine by Brendan Armstrong
//  Original KDE modifications by David Joham
//  Opera reload/resize based on a routine by Michael Wallner
//  Select-element hiding routine by Huy Do
//
///////////////////////////////////////////////////////////////////////////





// *** POSITIONING AND STYLES *********************************************



var menuALIGN = "left";		// alignment mode
var absLEFT = 	10;		// absolute left or right position (if menu is left or right aligned)
var absTOP = 	0; 		// absolute top position

var staticMENU = false;		// static positioning mode (win/ie5,ie6 and ns4 only)

var stretchMENU = false;	// show empty cells
var showBORDERS = false;	// show empty cell borders

var baseHREF = "http://www.nd.edu/~mcobweb/js/";	// base path
var zORDER = 	1000;		// base z-order of nav structure

var mCOLOR = 	"#FFFFFF";	// main nav cell color
var rCOLOR = 	"#FFFFFF";	// main nav cell rollover color
var bSIZE = 	0;		// main nav border size
var bCOLOR = 	"#003366";	// main nav border color
var aLINK = 	"#000000";	// main nav link color
var aHOVER = 	"#FFCC00";		// main nav link hover-color (dual purpose)
var aDEC = 		"none";		// main nav link decoration
var fFONT = 	"verdana,arial,sans-serif";	// main nav font face
var fSIZE = 	9;		// main nav font size (pixels)
var fWEIGHT = 	"bold";		// main nav font weight
var tINDENT = 	7;		// main nav text indent (if text is left or right aligned)
var vPADDING = 	0;		// main nav vertical cell padding
var vtOFFSET = 	0;		// main nav vertical text offset (+/- pixels from middle)

var keepLIT =	false;		// keep rollover color when browsing menu
var vOFFSET = 	2;		// shift the submenus vertically
var hOFFSET = 	2;		// shift the submenus horizontally

var smCOLOR = 	"#DDDDDD";	// submenu cell color
var srCOLOR = 	"#FFFFFF";	// submenu cell rollover color
var sbSIZE = 	1;		// submenu border size
var sbCOLOR = 	"#003366";	// submenu border color
var saLINK = 	"#003366";	// submenu link color
var saHOVER = 	"#003366";	// submenu link hover-color (dual purpose)
var saDEC = 	"none";		// submenu link decoration
var sfFONT = 	"verdana,arial,sans-serif";	// submenu font face
var sfSIZE = 	9;		// submenu font size (pixels)
var sfWEIGHT = 	"normal";	// submenu font weight
var stINDENT = 	5;		// submenu text indent (if text is left or right aligned)
var svPADDING = 1;		// submenu vertical cell padding
var svtOFFSET = 0;		// submenu vertical text offset (+/- pixels from middle)

var shSIZE =	2;		// submenu dropshadow size
var shCOLOR =	"#999999";	// submenu dropshadow color
var shOPACITY = 100;		// submenu dropshadow opacity (not ie4,ns4 or opera)

var keepSubLIT = false;		// keep submenu rollover color when browsing child menu
var chvOFFSET = 2;		// shift the child menus vertically
var chhOFFSET = 7;		// shift the child menus horizontally

var openTIMER = 0;		// menu opening delay time (not ns4/op5/op6)
var openChildTIMER = 0;	// child-menu opening delay time (not ns4/op5/op6)
var closeTIMER = 0;		// menu closing delay time

var aCURSOR = "hand";		// cursor for active links (not ns4, op5 or op6)
var altDISPLAY = "";		// where to display alt text
var allowRESIZE = mu;		// allow resize/reload

var redGRID = false;		// show a red grid
var gridWIDTH = 0;		// override grid width
var gridHEIGHT = 0;		// override grid height
var documentWIDTH = 0;		// override document width

var hideSELECT = true;		// auto-hide select boxes when menus open (ie only)
var allowForSCALING = true;	// allow for text scaling in gecko browsers
var allowPRINTING = false;	// allow the navbar and menus to print (not ns4)

var arrWIDTH = 13;		//arrow width (not ns4/op5/op6)
var arrHEIGHT = 13;		//arrow height (not ns4/op5/op6)

var arrHOFFSET = -1;		//arrow horizontal offset (not ns4/op5/op6)
var arrVOFFSET = 0;		//arrow vertical offset (not ns4/op5/op6)
var arrVALIGN = "middle";	//arrow vertical align (not ns4/op5/op6)

var arrLEFT = "<";		//left arrow (not ns4/op5/op6)
var arrLEFT_ROLL = "";		//left rollover arrow (not ns4/op5/op6)
var arrRIGHT = ">";		//right arrow (not ns4/op5/op6)
var arrRIGHT_ROLL = "";		//right rollover arrow (not ns4/op5/op6)





//** LINKS ***********************************************************

MI("http://www.nd.edu/~mcobweb/","cobweb",100,"center","","COBWeb",0,0,"","","","","");
	SP(120,"left","left",0,0,"","","","","","","");
	
MI("","&#8226;",5,"center","","dot",0,0,"","","","","");

MI("http://www.business.nd.edu/cobweb/","intranet",100,"center","","COBWeb",0,0,"","","","","");

MI("","&#8226;",5,"center","","dot",0,0,"","","","","");

MI("http://www.nd.edu/","notre dame",110,"center","","NOTRE DAME",0,0,"","","","","");
	SP(120,"left","left",0,0,"","","","","","","");
	
MI("","&#8226;",5,"center","","dot",0,0,"","","","","");

MI("http://www.nd.edu/~cba/","mendoza",110,"center","","MENDOZA",0,0,"","","","","");
	SP(225,"left","left",0,0,"","","","","","","");

	SI("http://www.nd.edu/~cba/011221/about/index.shtml","About Mendoza","","About Mendoza");
	SI("http://www.nd.edu/~cba/011221/programs/index.shtml","Programs","","Programs");
	SI("http://www.nd.edu/~cba/011221/research/index.shtml","Academic Centers & Special Initiatives","","Academic Centers & Special Initiatives");
	SI("http://www.nd.edu/~cba/011221/contact/index.shtml","Contact Us","","Contact Us");
	SI("http://www.nd.edu/~cba/011221/visit/index.shtml","Visit Mendoza","","Visit Mendoza");

MI("","&#8226;",5,"center","","dot",0,0,"","","","","");

// add main link item ("url","Link name",width,"text-alignment","_target","alt text",top position,left position,"key trigger","mCOLOR","rCOLOR","aLINK","aHOVER")
MI("","programs",110,"center","","programs",0,0,"","","","","");

	// define submenu properties (width,"align to edge","text-alignment",v offset,h offset,"filter","smCOLOR","srCOLOR","sbCOLOR","shCOLOR","saLINK","saHOVER")
	SP(120,"left","left",0,0,"","","","","","","");
	
	// add submenu link items ("url","Link name","_target","alt text")
	SI("","Undergraduate","","Undergraduate");
		CP(100,"left","center",0,-10,"","","","","","","");
		    CI("http://www.nd.edu/~acctdept","Accountancy","","");
			CI("http://www.nd.edu/~finance/","Finance","","");
			CI("http://www.nd.edu/~markdept/","Marketing","","");
			CI("http://www.nd.edu/~mgtdept/","Management","","");
			
	SI("","Graduate","","Graduate");
		CP(100,"left","center",0,-10,"","","","","","","");
			CI("http://www.nd.edu/~execprog/","EMBA","","");
		    CI("http://www.nd.edu/~mba/","MBA","","");
			CI("http://www.nd.edu/~msa/","MNA","","");
			CI("http://www.nd.edu/~msacct/","MSAcct","","");

	SI("","Centers","","Centers");
		CP(270,"left","center",0,-10,"","","","","","","");
			CI("http://www.nd.edu/~ethbus/","Institute for Ethical Business Worldwide","","");
		    CI("http://www.nd.edu/~ethics/","Center for Ethics and Religious Values","","");
			CI("http://www.nd.edu/~entrep/","Gigot Center for Entrepreneurial Studies","","");
			CI("http://www.nd.edu/~fanning/","Fanning Center for Business Communication","","");
			CI("http://www.nd.edu/~carecob/","Center for Accounting Research and Education","","");
			CI("http://www.nd.edu/~cba/011221/research/cfujs.shtml","Center for US-Japanese Studies","","");

MI("","&#8226;",5,"center","","dot",0,0,"","","","","");

// add main link item ("url","Link name",width,"text-alignment","_target","alt text",top position,left position,"key trigger","mCOLOR","rCOLOR","aLINK","aHOVER")
MI("","directories",110,"center","","directories",0,0,"","","","","");

	// define submenu properties (width,"align to edge","text-alignment",v offset,h offset,"filter","smCOLOR","srCOLOR","sbCOLOR","shCOLOR","saLINK","saHOVER")
	SP(130,"left","left",0,0,"","","","","","","");

	// add submenu link items ("url","Link name","_target","alt text")
	SI("http://www.business.nd.edu/mcob/faculty/alllistalpha.cfm","Faculty & Staff","","Faculty & Staff");
	SI("http://apps.nd.edu/webdirectory/","Notre Dame Directory","","Notre Dame Directory");
	
	// define child menu properties (width,"align to edge","text-alignment",v offset,h offset,"filter","smCOLOR","srCOLOR","sbCOLOR","shCOLOR","saLINK","saHOVER")
//CP(140,"left","center",0,-10,"","","","","","","");

		// add child menu link items ("url","Link name","_target","alt text")
		//CI("http://www.nd.edu/~ethbus/One_Year.htm","Cardinal O'Hara Series","","");
		//CI("http://www.nd.edu/~ethbus/Two_Year.htm","Martin Series","","");
	
	
	//SI("http://www.nd.edu/~ethbus/researchScholarship/publications.htm","NOtre Dame Directory","","Publications");
	//SI("http://www.nd.edu/~ethbus/researchScholarship/news.htm","News","","News");
	//SI("http://www.nd.edu/~ethbus/researchScholarship/ndInitiatives.htm","ND Initiatives","","ND Initiatives");

		// define child menu properties (width,"align to edge","text-alignment",v offset,h offset,"filter","smCOLOR","srCOLOR","sbCOLOR","shCOLOR","saLINK","saHOVER")
//		CP(178,"left","center",0,0,"","","","","","","");

		// add child menu link items ("url","Link name","_target","alt text")
//		CI("http://www.brothercake.com/dropdown/custom.php?v=menuALIGN","Global style definitions","","");


	
	
		
	

//* DO NOT EDIT THIS BIT *
}//***********************
//************************

